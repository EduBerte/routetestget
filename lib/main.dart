import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'pagina2.dart';
import 'pagina3.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    String jsonString = '''
    {
      "rotas": [
        {"name": "/second", "screen": "SecondScreen"},
        {"name": "/third", "screen": "ThirdScreen"}
      ]
    }
    ''';

    Map<String, dynamic> jsonData = json.decode(jsonString);

    final Map<String, WidgetBuilder> myRoutes = {};
    for (var rota in jsonData['rotas']) {
      myRoutes[rota['name']] = (context) {
        switch (rota['screen']) {
          case 'SecondScreen':
            return const SecondScreen();
          case 'ThirdScreen':
            return const ThirdScreen();
          default:
            return Container();
        }
      };
    }

    return GetMaterialApp(
      routes: myRoutes,
      home: const HomeScreen(),
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Home')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text("Home Screen"),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Get.toNamed('/second');
              },
              child: const Text('Go to Second Screen'),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                Get.toNamed('/third');
              },
              child: const Text('Go to Third Screen'),
            ),
          ],
        ),
      ),
    );
  }
}
